'use strict'

import React, {Component} from 'react';
import TodoItem from './TodoItem';


export default class ListMainSection extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let itemNodes = this.props.todos.map(
            t => <TodoItem listId={this.props.listId} key={t.id} id={t.id} text={t.text} complete={t.complete}/>
        );

        return (
            <div>
                {itemNodes}
            </div>
            );
    }
}
