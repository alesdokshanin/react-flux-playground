'use strict';

import React, {Component} from 'react';
import {dispatch} from '../core/TodoDispatcher';

const ENTER_KEY_CODE = 13;

export default class ListHeading extends Component {
    constructor(props) {
        super(props);
        this.state = {newTitle: this.props.title, isEditing: false, shouldSelectText: false};

        this._textOnClick = this._textOnClick.bind(this);
        this._onChange = this._onChange.bind(this);
        this._onBlur = this._onBlur.bind(this);
        this._changeTitle = this._changeTitle.bind(this);
        this._onKeyDown = this._onKeyDown.bind(this);
    }

    _textOnClick(e) {
        if(!this.state.isEditing) {
            this.setState({isEditing: true, shouldSelectText: true});
        }
    }

    componentDidUpdate() {
        if(this.state.shouldSelectText) {
            React.findDOMNode(this.refs.titleInput).select();
            this.setState({shouldSelectText: false})
        }
    }

    _onChange(e) {
        this.setState({newTitle: e.target.value});
    }

    _changeTitle() {
        this.props.renameList(this.state.newTitle);
        this.setState({isEditing: false});
    }

    _onBlur(e) {
        this._changeTitle();
    }

    _onKeyDown(e) {
        if(e.keyCode === ENTER_KEY_CODE) {
            this._changeTitle();
        }
    }

    render() {
        var titleStyle = {display: this.state.isEditing ? 'none' : 'block'};
        var inputStyle = {display: this.state.isEditing ? 'block' : 'none'};

        return (
            <li className="list-group-item active">
                <input
                    ref="titleInput"
                    onKeyDown={this._onKeyDown}
                    className="form-control"
                    onBlur={this._onBlur}
                    onChange={this._onChange}
                    type="text"
                    style={inputStyle}
                    value={this.state.newTitle} />

                <h4 
                    style={titleStyle}
                    onClick={this._textOnClick}
                    className="noselect">
                    {this.props.title}
                </h4>

            </li>
            );
    }
}
