'use strict';

import {Container} from 'flux/utils';
import React, {Component} from 'react';

import TodoStore from '../core/TodoStore';
import TodoList from './TodoList';
import {dispatch} from '../core/TodoDispatcher';

export default class TodoApp extends Component {

    static getStores() {
        return [TodoStore];
    }

    static calculateState(state) {
        return {
            lists: TodoStore.getState(),
        }
    }

    _onAddClick(e) {
        dispatch({
            type: 'todolist/create'
        });
    }

    render() {
        let listNodes = this.state.lists.map(l => <TodoList key={l.id} list={l}/>)

        return (
            <div>
                <div className="page-header app-header">
                    <h1>FLUX + React.js TODO Application</h1>
                </div>
        
                <div className="app-content">
                    {listNodes}

                    <div className="col-sm-4 col-md-3 add-new-list">
                        <button onClick={this._onAddClick} className="btn btn-primary">Add new list</button>
                    </div>
                </div>


            </div>
            );
    }
}

const TodoAppContainer = Container.create(TodoApp);
export default TodoAppContainer;
