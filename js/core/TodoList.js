'use strict'

import Immutable from 'immutable';
import {generateId} from './utils';

const TodoListRecord = Immutable.Record({
    id: undefined,
    title: undefined,
    todos: undefined,
});

export default class TodoList extends TodoListRecord {
    id;
    title;
    todos;

    constructor(title='New List Default') {
        super({
            id: generateId(),
            todos: Immutable.OrderedMap(),
            title: title,
        });
    }
}
